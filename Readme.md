

## Markdown to PDF pipeline

```bash
pandoc Octomode/pad.md -o index.html
```

add `-s` to produce standalone HTML output

see template to add styles and paged.js polyfill  
https://pandoc.org/MANUAL.html#templates

for now:  manually insert Pandoc output in a full HTML page with paged.js and stylesheets added 